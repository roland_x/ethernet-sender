import time
from PIL import ImageGrab,Image,ImageTk
import cStringIO
import io
import Tkinter
from threading import Thread
import packetParser


class TkWindow():
    def __init__(self,delay):
        self.root = Tkinter.Tk()
        self.label = Tkinter.Label(self.root)
        self.label.pack()
        # self.img = None
        self.tkimg = [None]  # This, or something like it, is necessary because if you do not keep a reference to PhotoImage instances, they get garbage collected.

        self.delay = delay   # in milliseconds

        self.displayData=""


    def __loopCapture(self):
        print "capturing"
        # fakie = cStringIO.StringIO()
        # ImageGrab.grab().save(fakie, 'PNG') #bbox specifies specific region (bbox= x,y,width,height *starts top-left)
        # data = fakie.getvalue().encode("hex")

        # data=''.join(packetParser.hexDataList)

        # data=packetParser.goodScreen.get()
        # with open("1.png","wb") as f:
        #     f.write(data.decode("hex"))
        # print "WRITE DATA"
        if len(packetParser.goodScreen)>0:
            print len(packetParser.goodScreen)
            self.displayData=packetParser.goodScreen.pop(0)

        try:
            image = Image.open(io.BytesIO(self.displayData.decode("hex")))
            self.tkimg[0] = ImageTk.PhotoImage(image)
            self.label.config(image=self.tkimg[0])
        except:
            # image= Image.open("tests/test2.png")
            # self.tkimg[0] = ImageTk.PhotoImage(image)
            pass
        self.root.update_idletasks()
        self.root.after(self.delay, self.__loopCapture)

        



    def run(self):
        mainThread = Thread(target=self.__run)
        mainThread.start()

    def __run(self):
        self.__loopCapture()
        self.root.mainloop()

    def close(self):
        print "close"
        # self.mainThread.join()
        self.root.destroy()



# tk=TkWindow(500)
# tk.run()
# tk.close()
