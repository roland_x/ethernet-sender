import sys
from PyQt4 import QtGui, uic
from PyQt4.QtCore import *
from PyQt4 import QtCore, QtGui
import qdarkstyle
import time
from constants import *
import packetSender
from threading import Lock, Thread, Event
from winpcapy import WinPcapUtils
import packetParser
from utils import *
from tkScreen import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class InterfaceThread(QThread):
    update_line_edit = pyqtSignal(str)
    def __init__(self):
        super(InterfaceThread,self).__init__() 
        
    def run(self):
        while packetSender.PROGRAM_WORKING:
            self.temp = "123"
            self.update_line_edit.emit(self.temp)
            time.sleep(0.1)


class MyWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        uic.loadUi('app.ui', self)
        self.show()

        self.interfaceThread=InterfaceThread()
        self.interfaceThread.update_line_edit.connect(self.interfaceThreadHandler)
        self.interfaceThread.start()

        self.listenThread = Thread(target=self.startListen)
        self.listenThread.start()

        self.broadcastSendThread = Thread(target=packetSender.sendBroadcastMessage)
        self.broadcastSendThread.start()

        self.sendPacketsThread = Thread(target=packetSender.sendPacketsThread)
        self.sendPacketsThread.start()

        self.chatWindowData=dict()
        self.currentUser=None

        self.printedUsers=list()

        self.tkScreenShare=TkWindow(300)



    def setupUi(self):
        # self.resize(606, 515)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Comic Sans MS"))
        font.setPointSize(12)
        self.setFont(font)

        self.list_onlineUsers.itemSelectionChanged.connect(self.selectionChanged)

        self.btn_sendMessage.clicked.connect(self.sendMessageHandler)
        self.btn_attachFile.clicked.connect(self.attachFileHandler)
        self.btn_settings.clicked.connect(self.showSettings)
        self.btn_shareScreen.clicked.connect(self.shareScreen)

        # add const users
        for item in packetParser.onlineUsersList:
            userName,userMac=item.split(":")
            self.list_onlineUsers.addItem(userName)
            # store user 
            self.printedUsers.append(item)



        self.list_onlineUsers.setItemSelected(self.list_onlineUsers.item(0), True)

        self.label_shareStatus.setStyleSheet("color: red;")

    # -----------------------------------------
    #   INTERFACE THREAD HANDLER
    # -----------------------------------------
    def interfaceThreadHandler(self):
        # check messages queue and print
        if packetParser.textPacketsQueue.qsize()>0:
            packet=packetParser.textPacketsQueue.get()
            infoStr=packet["src_mac"]+": "+packet["data"].decode("hex")
            self.tb_chat.setText(_translate("MainWindow", self.tb_chat.toPlainText()+infoStr+'\n', None))

        # check new users
        if len(self.printedUsers)!=len(packetParser.onlineUsersList):
            for user in packetParser.onlineUsersList:
                if user in self.printedUsers:
                    continue
                userName,userMac=user.split(":")
                self.list_onlineUsers.addItem(userName)
                self.printedUsers.append(user)


    def selectionChanged(self):
        self.tb_chat.clear()
        selectedItem=self.list_onlineUsers.selectedItems()[0].text()

        for user in packetParser.onlineUsersList:
            userName,userMac=user.split(":")
            if userName==selectedItem:
                self.currentUser=userMac
        print "Selected - ",userName,userMac


    def sendMessageHandler(self):
        src_mac=Utils.getMyMac()
        dst_mac=self.currentUser
        packetSender.sendMessage(dst_mac,src_mac,str(self.tb_message.toPlainText()))
        self.tb_chat.clear()



        # data=str(self.tb_message.toPlainText())
        # filename = "tests/photo/python3.3.zip"

        # sendFile(dst_mac,src_mac,filename)

    def attachFileHandler(self):
        src_mac=Utils.getMyMac()
        dst_mac=self.currentUser
        # options = QtGui.QFileDialog.Options()
        # options = QtGui.QFileDialog.DontUseNativeDialog
        # fileName= QtGui.QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Select file for sending', '/')
        packetSender.sendFile(dst_mac,src_mac,str(filename))

    # -----------------------------------------

    #      LISTEN THREAD STARTING
    # -----------------------------------------
    def startListen(self):
        WinPcapUtils.capture_on("*Ethernet*", packetParser.parsePackets)

    def closeEvent(self, event):
        packetSender.PROGRAM_WORKING=False

    def showSettings(self):
        w=QtGui.QDialog()
        uic.loadUi('settingsDialog.ui',w)
        w.exec_()

    def shareScreen(self):
        if packetSender.SCREEN_SHARING==False:
            packetSender.SCREEN_SHARING=True
            self.label_shareStatus.setText("active")
            self.label_shareStatus.setStyleSheet("color: green;")

            self.shareScreen = Thread(target=packetSender.shareScreen)
            self.shareScreen.start()

            self.tkScreenShare.run()
            return

        if packetSender.SCREEN_SHARING:
            packetSender.SCREEN_SHARING=False
            self.label_shareStatus.setText("disabled")
            self.label_shareStatus.setStyleSheet("color: red;")

            self.tkScreenShare.close()
            return




if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyside())
    window = MyWindow()
    window.setupUi()
    sys.exit(app.exec_())