# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from packetSender import *
from threading import Lock, Thread, Event
import time
import packetParser
import constants
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class MyThread(QThread):
    update_line_edit = pyqtSignal(str)
    def __init__(self):
        super(MyThread,self).__init__() 
        
    def run(self):
        while True:
            self.temp = "123"
            self.update_line_edit.emit(self.temp)
            time.sleep(0.1)


class Ui_MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.newMessage=''
        self.listenThread = Thread(target=self.startListen)
        self.listenThread.start()

        self.broadcastSendThread = Thread(target=sendBroadcastMessage)
        self.broadcastSendThread.start()

        self.sendPacketsThread = Thread(target=sendPacketsThread)
        self.sendPacketsThread.start()

        app.aboutToQuit.connect(self.closeEvent)

        self.onlineMacList=["ffffffffffff"]
        self.inComboBoxMacs=list()

    
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(606, 515)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Comic Sans MS"))
        font.setPointSize(12)
        MainWindow.setFont(font)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 201, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 80, 251, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_myMac = QtGui.QLabel(self.centralwidget)
        self.label_myMac.setGeometry(QtCore.QRect(0, 60, 611, 20))
        self.label_myMac.setObjectName(_fromUtf8("label_myMac"))
        self.tb_chat = QtGui.QTextEdit(self.centralwidget)
        self.tb_chat.setGeometry(QtCore.QRect(10, 103, 591, 241))
        self.tb_chat.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.tb_chat.setReadOnly(True)
        self.tb_chat.setObjectName(_fromUtf8("tb_chat"))
        self.label_4 = QtGui.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(10, 350, 251, 21))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.tb_message = QtGui.QTextEdit(self.centralwidget)
        self.tb_message.setGeometry(QtCore.QRect(10, 370, 591, 61))
        self.tb_message.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.tb_message.setObjectName(_fromUtf8("tb_message"))
        self.btn_clear = QtGui.QPushButton(self.centralwidget)
        self.btn_clear.setGeometry(QtCore.QRect(10, 440, 101, 23))
        self.btn_clear.setObjectName(_fromUtf8("btn_clear"))
        self.btn_send = QtGui.QPushButton(self.centralwidget)
        self.btn_send.setGeometry(QtCore.QRect(500, 440, 101, 23))
        self.btn_send.setObjectName(_fromUtf8("btn_send"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(230, 10, 71, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.tb_dstMac = QtGui.QLineEdit(self.centralwidget)
        self.tb_dstMac.setGeometry(QtCore.QRect(310, 10, 141, 20))
        self.tb_dstMac.setObjectName(_fromUtf8("tb_dstMac"))
        self.cb_macList = QtGui.QComboBox(self.centralwidget)
        self.cb_macList.setGeometry(QtCore.QRect(310, 40, 161, 22))
        self.cb_macList.setObjectName(_fromUtf8("cb_macList"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 606, 18))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.btn_send.clicked.connect(self.sendClick)
        self.btn_clear.clicked.connect(self.clearClick)

        self.chatRefreshThread=MyThread()
        self.chatRefreshThread.update_line_edit.connect(self.interfaceThreadHandler)
        self.chatRefreshThread.start()



    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Ethernet chat", None))
        self.label.setText(_translate("MainWindow", "My mac - "+Utils.getMyMac(), None))
        self.label_2.setText(_translate("MainWindow", "Chat window:", None))
        self.label_myMac.setText(_translate("MainWindow", "____________________________________________________________________________", None))
        self.label_4.setText(_translate("MainWindow", "Message:", None))
        self.btn_clear.setText(_translate("MainWindow", "Clear", None))
        self.btn_send.setText(_translate("MainWindow", "Send", None))
        self.label_3.setText(_translate("MainWindow", "Dst mac -", None))

    def sendClick(self):
        src_mac=Utils.getMyMac()
        dst_mac=str(self.tb_dstMac.text())
        # data=str(self.tb_message.toPlainText())
        filename = "tests/photo/BorlandC.rar"

        sendFile(dst_mac,src_mac,filename)

        

    def clearClick(self):
        self.tb_message.clear()

    # -----------------------------------------
    #   INTERFACE THREAD HANDLER
    # -----------------------------------------
    def interfaceThreadHandler(self):
        self.tb_dstMac.setText(_translate("MainWindow", self.cb_macList.currentText(), None))
        if len(self.onlineMacList)>0:
            newMac=self.onlineMacList.pop()
            if newMac not in self.inComboBoxMacs:
                self.cb_macList.addItem(newMac)
                self.inComboBoxMacs.append(newMac)

        
        if self.newMessage!='':
            self.tb_chat.setText(_translate("MainWindow", self.tb_chat.toPlainText()+self.newMessage+'\n', None))
            self.newMessage=''
            
    # -----------------------------------------
    #   LISTEN THREAD STARTING
    # -----------------------------------------
    def startListen(self):
        WinPcapUtils.capture_on("*Ethernet*", packetParser.parsePackets)


    def closeEvent(self):
        PROGRAM_WORKING=False

        
if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
