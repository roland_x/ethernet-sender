#coding: utf-8
from winpcapy import WinPcapUtils
from utils import *
import os
from multiprocessing import Queue
import time
import constants
import random
import os
from threading import Thread
import cStringIO
from PIL import ImageGrab

PROGRAM_WORKING=True
SCREEN_SHARING=False

toSendPacketsQueue=Queue()


filesInfoDict=dict()

def sendFile(dst_mac,src_mac,fullFileName):
    # generate file id
    fileId=random.randint(1,200)
    fileId=Utils.fillBytes(hex(fileId)[2:],2) # to hex


    packet_data = {
        "dst_mac": dst_mac, # 6
        "src_mac": src_mac, # 6
        "type":constants.FILE_TYPE,  # 2
        "crc":"000055aa", # 4
        "lenght":"", # 2
        "number":"00"*4,    # 4
        "fileId":fileId, #2 
        "data": ""
    }

    fileSize=int(os.path.getsize(fullFileName))
    # get file name with type
    if fullFileName.find("/")!=-1:
        filename=fullFileName[fullFileName.rfind("/")+1:len(fullFileName)]

    # calc packets count
    packetCount=fileSize//constants.MAX_LENGHT
    if fileSize%constants.MAX_LENGHT>0: packetCount+=1
    packetCount=Utils.fillBytes(hex(packetCount)[2:],4) 

    # send service packet
    sendServicePacket(dst_mac,src_mac,fullFileName,packetCount,fileId,fileSize)

    if packetCount>1:
        splitterThread = Thread(target=__fileSlpitter,args=(packet_data, fileSize,fullFileName))
        splitterThread.start()
    return

# ---------------------------------------
#      SPLIT BIG PACKETS
# ---------------------------------------
def __fileSlpitter(packet,fileSize,fullFileName):
    # print "Start splitting file"
    newPacket=packet
    # data=packet["data"]
    
    # set count packets
    packetCount=fileSize//constants.MAX_LENGHT
    if fileSize%constants.MAX_LENGHT>0: packetCount+=1

    with open(fullFileName,"rb") as f:
        for packetNum in range(1,packetCount+1):
            # set offset amd read part of data
            f.seek((packetNum-1)*constants.MAX_LENGHT)
            partData=f.read(constants.MAX_LENGHT).encode("hex")

            # split data
            newPacket["data"]=partData
            newPacket["lenght"]=Utils.fillBytes(Utils.getLenght(partData),2) # 2
            newPacket["number"]=Utils.fillBytes(hex(packetNum)[2:],4) 

            # calculate crc
            crc=Utils.calculateCrc(newPacket["lenght"]+newPacket["number"]+newPacket["fileId"]+newPacket["data"])
            newPacket["crc"]=Utils.fillBytes(crc,4)

            # print newPacket["data"] 
            if newPacket["type"]==constants.FILE_TYPE: 
                toSendPacketsQueue.put(constants.FILE_PACKET_TEMPL% newPacket)



def sendScreen(dst_mac,src_mac,fileHexData):
    packet_data = {
        "dst_mac": dst_mac, # 6
        "src_mac": src_mac, # 6
        "type":constants.SCREEN_TYPE,  # 2
        "lenght":"", # 2
        "number":"00"*4,    # 4
        "lastNumber":"00"*4,
        "data": ""
    }
    screenSize=int(Utils.getLenght(fileHexData),16)

    # calc packets count
    packetCount=screenSize//constants.MAX_LENGHT
    if screenSize%constants.MAX_LENGHT>0: packetCount+=1
    packetCount=Utils.fillBytes(hex(packetCount)[2:],4)

    # send service packet
    # sendServicePacket(dst_mac,src_mac,"1.png",packetCount,fileId,fileSize)

    splitterThread = Thread(target=__dataSplitter,args=(packet_data, screenSize,fileHexData))
    splitterThread.start()


def __dataSplitter(packet,dataSize,fileHexData):
    newPacket=packet
    # set count packets
    packetCount=dataSize//constants.MAX_LENGHT
    if dataSize%constants.MAX_LENGHT>0: packetCount+=1
    for packetNum in range(1,packetCount+1):
        # split data
        partData=fileHexData[(packetNum-1)*(constants.MAX_LENGHT*2):packetNum*(constants.MAX_LENGHT*2)]
        newPacket["data"]=partData
        newPacket["lenght"]=Utils.fillBytes(Utils.getLenght(partData),2) # 2
        newPacket["number"]=Utils.fillBytes(hex(packetNum)[2:],4)
        newPacket["lastNumber"]=Utils.fillBytes(hex(packetCount)[2:],4)
        # calculate crc
        # crc=Utils.calculateCrc(newPacket["lenght"]+newPacket["number"]+newPacket["fileId"]+newPacket["data"])
        # if packetNum==43:
        #     print partData.strip()
        toSendPacketsQueue.put(constants.SCREEN_PACKET_TEMPL% newPacket)

        # print json.dumps(newPacket, indent = 4)

    # print "End splitting"
    return



# ---------------------------------------
#      SEND SERVICE PACKETS
# ---------------------------------------
def sendServicePacket(dst_mac,src_mac,fullFileName,packetCount,fileId,fileSize):
    filename=fullFileName
    # get file name with type
    if fullFileName.find("/")!=-1:
        filename=fullFileName[fullFileName.rfind("/")+1:len(fullFileName)]

    data=filename.encode("hex")

    packet_data={
        "dst_mac": dst_mac, #6
        "src_mac": src_mac, #6
        "type":constants.SERVICE_TYPE,  #2
        "crc":"000055aa", # 4
        "lenght":Utils.fillBytes(Utils.getLenght(data),2), # 2
        "packetCount":packetCount, #4
        "fileId":fileId, #2
        "fileSize":Utils.fillBytes(hex(fileSize)[2:],4), # 4
        "filename":data
    }

    # calculate crc
    crc=Utils.calculateCrc(packet_data["lenght"]+packet_data["packetCount"]+packet_data["fileId"]
        +packet_data["fileSize"]+packet_data["filename"])
    packet_data["crc"]=Utils.fillBytes(crc,4)

    filesInfoDict[fileId]=fullFileName

    packet = constants.SERVICE_PACKET_TEMPL% packet_data
    toSendPacketsQueue.put(packet)


# ---------------------------------------
#      SEND TEXT PACKETS
# ---------------------------------------
def sendMessage(dst_mac,src_mac,data,info=False):
    data=data.encode("hex")
    packet_data={
        "dst_mac": dst_mac, #6
        "src_mac": src_mac, #6
        "type":constants.MESSAGE_TYPE,  #2
        "lenght":Utils.fillBytes(Utils.getLenght(data),2), # 2
        "number":Utils.fillBytes("00",4), #4
        "data":data
    }
    if info==True:
        packet_data["type"]=constants.INFO_TYPE

    packet = constants.TEXT_PACKET_TEMPL% packet_data
    toSendPacketsQueue.put(packet)


def sendRequestPacket(dst_mac,src_mac,fileId,packetNumber):
    packet_data={
        "dst_mac": dst_mac, #6
        "src_mac": src_mac, #6
        "type":constants.REQUEST_TYPE,  #2
        "crc":"000055aa", # 4
        "fileId":fileId, #2
        "packetNumber":packetNumber, #4
    }

    # calculate crc
    crc=Utils.calculateCrc(packet_data["fileId"]+packet_data["packetNumber"])
    packet_data["crc"]=Utils.fillBytes(crc,4)

    packet = constants.REQUEST_PACKET_TEMPL% packet_data
    toSendPacketsQueue.put(packet)



def sendPacketsThread():
    while PROGRAM_WORKING:
        packet=toSendPacketsQueue.get()
        WinPcapUtils.send_packet("*Ethernet*", packet.decode("hex"))

def sendBroadcastMessage():
    while PROGRAM_WORKING:
        src_mac=Utils.getMyMac()
        dst_mac='ffffffffffff'
        data=USER_NAME
        sendMessage(dst_mac,src_mac,data,True)
        time.sleep(3)


def shareScreen():
    src_mac=Utils.getMyMac()
    dst_mac='ffffffffffff'
    while PROGRAM_WORKING and SCREEN_SHARING:
        # grab image and store in memory
        # fakie = cStringIO.StringIO()
        # ImageGrab.grab().save(fakie, 'PNG') #bbox specifies specific region (bbox= x,y,width,height *starts top-left)
        # data=fakie.getvalue().encode("hex")
        # print "len data",(len(data)/2)//1024

        image = ImageGrab.grab()
        buffer = cStringIO.StringIO()
        image.save(buffer, "PNG")
        data = buffer.getvalue().encode("hex")
        buffer.close()

        # open('screen_capture.png', 'w').close()
        # ImageGrab.grab().save("screen_capture.png", "PNG")
        # with open("monitor-1.png","rb") as f:
        #     data=f.read().encode("hex")


        sendScreen(dst_mac,src_mac,data)
        time.sleep(0.1)
        # return



# src_mac=Utils.getMyMac()
# dst_mac='ffffffffffff'
# # grab image and store in memory
# fakie = cStringIO.StringIO()
# ImageGrab.grab().save(fakie, 'PNG') 
# data=fakie.getvalue().encode("hex")
# print "len data",(len(data)/2)//1024
# sendFile(dst_mac,src_mac,fileHexData=data,protocolType=constants.SCREEN_TYPE)