from PIL import ImageGrab
import numpy as np
import cv2
from cPickle import dumps, loads
import time
import cStringIO
from PIL import Image
import io

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print '%r (%r, %r) %2.2f sec' % \
              (method.__name__, args, kw, te-ts)
        return result

    return timed

@timeit
def test():
    while True:
        fakie = cStringIO.StringIO()
        ImageGrab.grab().save(fakie, 'PNG') #bbox specifies specific region (bbox= x,y,width,height *starts top-left)
        
        data = fakie.getvalue().encode("hex")
        # image = Image.frombytes('RGBA', (128,128), data.decode("hex"), 'raw')

        image = Image.open(io.BytesIO(data.decode("hex")))
        # image.show()

        cv2.imshow("test", np.array(image))
        cv2.waitKey(0)
        # cv2.destroyAllWindows()
        time.sleep(1)

    # with open("test2.png","wb") as f:
    #     f.write(data.decode("hex"))

        # img_np = np.array(data.decode("hex")) #this is the array obtained from conversion
        #data=dumps(img_np)
        # frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)

        # counter+=1
        

test()
