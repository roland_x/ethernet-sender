import numpy as np
import cv2
from PIL import ImageGrab
import cStringIO
import time

last_time = time.time()
while(True):
    # screen = ig.grab()
    image = ImageGrab.grab()
    buffer = cStringIO.StringIO()
    image.save(buffer, "PNG")
    data = buffer.getvalue().encode("hex")
    buffer.close()

    print('Loop took {} seconds',format(time.time()-last_time))

    cv2.imshow("test", np.array(data.decode("hex")))
    last_time = time.time()
    time.sleep(0.1)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break


# f=open(r'c:\Documents and Settings\Admin\Рабочий стол\work\ethernet-sender\ethernet sender\monitor-1.png','rb')