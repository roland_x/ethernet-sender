import socket
import os
import threading
from PIL import ImageGrab
import time

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print '%r (%r, %r) %2.2f sec' % \
              (method.__name__, args, kw, te-ts)
        return result

    return timed


@timeit
def RetrFile(counter=0):
    counter=0
    while counter<25:
        img = ImageGrab.grab()
        img.save("photo/test.jpg","JPEG")

        filename = "photo/test.jpg"
        with open(filename, 'rb') as f:
            bytesToSend = f.read(488)
        counter+=1
            
        # img.show()


RetrFile(0)