import matplotlib.pyplot as plt
import numpy as np
import time
from matplotlib import animation
from PIL import ImageGrab,Image,ImageTk
import cv2
from cPickle import dumps, loads
import time
import cStringIO
import io
import Tkinter
import netius

# data = np.random.rand(128, 128)

# fig = plt.figure()
# ax = fig.add_subplot(1,1,1)

# im = ax.imshow(data, animated=True)

# def update_image(i):
#     fakie = cStringIO.StringIO()
#     ImageGrab.grab().save(fakie, 'PNG') #bbox specifies specific region (bbox= x,y,width,height *starts top-left)
#     data = fakie.getvalue().encode("hex")
#     image = Image.open(io.BytesIO(data.decode("hex")))

#     im.set_array(np.array(image))


#     # time.sleep(.5)
#     plt.pause(0.5)
# ani = animation.FuncAnimation(fig, update_image, interval=0)

# plt.show()

root = Tkinter.Tk()
label = Tkinter.Label(root)
label.pack()
img = None
tkimg = [None]  # This, or something like it, is necessary because if you do not keep a reference to PhotoImage instances, they get garbage collected.

delay = 500   # in milliseconds
def loopCapture():
    print "capturing"
    
    # fakie = cStringIO.StringIO()
    # ImageGrab.grab().save(fakie, 'PNG') #bbox specifies specific region (bbox= x,y,width,height *starts top-left)
    # data = fakie.getvalue().encode("hex")
    # image = Image.open(io.BytesIO(data.decode("hex")))


    image = ImageGrab.grab()
    buffer = netius.legacy.BytesIO()
    image.save(buffer, "JPEG")
    data = buffer.getvalue().encode("hex")+"00000000"*100
    image=Image.open(io.BytesIO(data.decode("hex")))
    tkimg[0] = ImageTk.PhotoImage(image)
    label.config(image=tkimg[0])
    root.update_idletasks()
    root.after(delay, loopCapture)

loopCapture()
root.mainloop()