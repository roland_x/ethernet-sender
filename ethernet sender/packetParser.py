from constants import *
from utils import *
import json
from multiprocessing import Queue
import crcmod
import os
from threading import Thread
import packetSender
import time
from PIL import ImageGrab,Image
import cv2
import cStringIO
import io
import numpy as np

textPacketsQueue=Queue()

fileDict=dict()

# queue for broadcast info packets
infoPackets=Queue()

# queue for request bad packets from other client
requestQueue=Queue()

# onlineUsersDict={"ffffffffffff":"Broadcast"}
onlineUsersList=["Broadcast:ffffffffffff"]
screenDataStr=""

goodScreen=list()


def __parseMessage(packet,output=False):
    packet_data={
        "dst_mac":packet[0:6].encode('hex'),
        "src_mac":packet[6:12].encode('hex'),
        "type":packet[12:14].encode('hex'),
        "lenght":packet[14:16].encode('hex'),
        "number":packet[16:20].encode('hex'),
        "data":packet[20:20+int(packet[14:16].encode('hex'), 16)].encode('hex')
        }

    if output==True:
        print "MESSAGE"
        print json.dumps(packet_data, indent = 4)

    return packet_data


def __parseServicePacket(packet,output=False):
    packet_data={
        "dst_mac":packet[0:6].encode('hex'), #6
        "src_mac":packet[6:12].encode('hex'), #6
        "type":packet[12:14].encode('hex'),  #2
        "crc":packet[14:18].encode('hex'),  # 4
        "lenght":packet[18:20].encode('hex'), # 2
        "packetCount":packet[20:24].encode('hex'), #4
        "fileId":packet[24:26].encode('hex'), #2
        "fileSize":packet[26:30].encode('hex'), # 4
        "filename":packet[30:30+int(packet[18:20].encode('hex'), 16)].encode('hex')
    }

    if output==True:
        print "SERVICE"
        print json.dumps(packet_data, indent = 4)

    return packet_data

def __parseScreenPacket(packet,output=False):
    packet_data={
        "dst_mac":packet[0:6].encode('hex'),
        "src_mac":packet[6:12].encode('hex'),
        "type":packet[12:14].encode('hex'),
        "lenght":packet[14:16].encode('hex'),
        "number":packet[16:20].encode('hex'),
        "lastNumber":packet[20:24].encode('hex'),
        "data":packet[24:24+int(packet[14:16].encode('hex'), 16)].encode('hex')
    }

    if output==True:
        print "SCREEN"
        print json.dumps(packet_data, indent = 4)

    return packet_data



def __parseFilePacket(packet,output=False):
    packet_data={
        "dst_mac":packet[0:6].encode('hex'),
        "src_mac":packet[6:12].encode('hex'),
        "type":packet[12:14].encode('hex'),
        "crc":packet[14:18].encode('hex'),
        "lenght":packet[18:20].encode('hex'),
        "number":packet[20:24].encode('hex'),
        "fileId":packet[24:26].encode('hex'),
        "data":packet[26:26+int(packet[18:20].encode('hex'), 16)].encode('hex')
    }

    if output==True:
        print "FILE"
        print json.dumps(packet_data, indent = 4)

    return packet_data

def __parseRequestPacket(packet,output=False):
    packet_data={
        "dst_mac":packet[0:6].encode('hex'),
        "src_mac":packet[6:12].encode('hex'),
        "type":packet[12:14].encode('hex'),
        "crc":packet[14:18].encode('hex'),
        "fileId":packet[18:20].encode('hex'),
        "packetNumber":packet[20:24].encode('hex'),
    }

    if output==True:
        print "REQUEST"
        print json.dumps(packet_data, indent = 4)

    return packet_data


# ---------------------------------------
#      SERVICE PACKETS HANDLER
# ---------------------------------------
def __servicePacketHandler(packet):
    if Utils.checkCrc(packet)==False:
        print "Service packet is not valid"
        return

    # create dict for new file
    fileDict[packet["fileId"]]=dict()
    fileDict[packet["fileId"]]["filename"]=packet["filename"]
    fileDict[packet["fileId"]]["from"]=packet["src_mac"]
    fileDict[packet["fileId"]]["filesize"]=packet["fileSize"]
    fileDict[packet["fileId"]]["packetCount"]=packet["packetCount"]
    fileDict[packet["fileId"]]["waitingPackets"]=[Utils.fillBytes(hex(i)[2:],4) for i in range(1,int(packet["packetCount"],16)+1)]

    # create temp file
    tempFilename=packet["filename"].decode("hex")+".tmp"
    print "Create "+tempFilename
    with open("received/"+tempFilename, "wb") as out:
        out.seek((int(packet["fileSize"],16)) - 1)
        out.write('\0')

    fileDict[packet["fileId"]]["writer"]=open("received/"+tempFilename,"r+b")


    # create thread for request bad packets
    reuqestBadPacketsThread = Thread(target=__badPacketsHandler,args=(packet["fileId"],))
    reuqestBadPacketsThread.start()
    # __badPacketsHandler(packet["fileId"])


# ---------------------------------------
#      PACKET HANLDERS
# ---------------------------------------
def __filePacketHandler(packet):
    if Utils.checkCrc(packet)==False:
        print "File packet is not valid"
        return

    filename=fileDict[packet["fileId"]]["filename"].decode("hex")

    # remove packet number from waiting list
    try:
        fileDict[packet["fileId"]]["waitingPackets"].remove(packet["number"])
    except ValueError:
        return

    packetNum=int(packet["number"],16)-1
    print "---",packetNum

    # write packet data to file
    fileDict[packet["fileId"]]["writer"].seek(MAX_LENGHT*packetNum)
    fileDict[packet["fileId"]]["writer"].write(packet["data"].decode("hex"))
# ---------------------------------------
def __badPacketsHandler(fileId):
    time.sleep(WAITING_PACKETS_TIMEOUT)
    while len(fileDict[fileId]["waitingPackets"])!=0:
        try:
            packetNumber=fileDict[fileId]["waitingPackets"][0]
            packetSender.sendRequestPacket(fileDict[fileId]["from"],Utils.getMyMac(),fileId,packetNumber)
        except IndexError:
            break

        time.sleep(0.1)
    print "File "+fileDict[fileId]["filename"].decode("hex")+" received"
    # create rename tmp file
# ---------------------------------------
def __requestPacketHandler(packet):
    if Utils.checkCrc(packet)==False:
        print "Request packet is not valid"
        return

    with open(packetSender.filesInfoDict[packet["fileId"]],"rb") as f:
        packetNum=int(packet["packetNumber"],16)
        f.seek(MAX_LENGHT*(packetNum-1))
        data=f.read(MAX_LENGHT).encode("hex")

        packet_data = {
            "dst_mac": packet["src_mac"], # 6
            "src_mac": Utils.getMyMac(), # 6
            "type":FILE_TYPE,  # 2
            "crc":"000055aa", # 4
            "lenght":Utils.fillBytes(Utils.getLenght(data),2), # 2
            "number":packet["packetNumber"],    # 4
            "fileId":packet["fileId"], #2 
            "data": data
        }

        # calculate crc
        crc=Utils.calculateCrc(packet_data["lenght"]+packet_data["number"]+packet_data["fileId"]+packet_data["data"])
        packet_data["crc"]=Utils.fillBytes(crc,4)


        packetSender.toSendPacketsQueue.put(FILE_PACKET_TEMPL% packet_data)
# ---------------------------------------
def __infoPacketHandler(packet):
    user=packet["data"].decode("hex")+":"+packet["src_mac"]
    if user in onlineUsersList:
        return
    onlineUsersList.append(user)

# ---------------------------------------
def __messagePacketHandler(packet):
    textPacketsQueue.put(packet)

def __screenPacketHandler(packet):
    global screenDataStr
    # filename=fileDict[packet["fileId"]]["filename"].decode("hex")

    packetNum=int(packet["number"],16)-1
    # hexDataList[packetNum]=packet["data"]
    screenDataStr+=packet["data"]
    # print packetNum
    if packet["number"]==packet["lastNumber"]:
        print len(goodScreen)
        goodScreen.append(screenDataStr)
        screenDataStr=""

    
    # print "---",packetNum
    # print shareScreenData
    # time.sleep(10)



    
    # # write packet data to file
    # fileDict[packet["fileId"]]["writer"].seek(MAX_LENGHT*packetNum)
    # fileDict[packet["fileId"]]["writer"].write(packet["data"].decode("hex"))


# ---------------------------------------
#      PACKETS PARSER
# ---------------------------------------
def parsePackets(win_pcap, param, header,pkt_data):
    #filter packet from current mac
    # src_mac=pkt_data[6:12].encode('hex')
    # if src_mac==Utils.getMyMac():
    #     return

    packetType = pkt_data[12:14].encode('hex')

    if packetType==SCREEN_TYPE:
        packet=__parseScreenPacket(pkt_data,False)
        __screenPacketHandler(packet)

    # parse messages packets
    if packetType == MESSAGE_TYPE:
        packet=__parseMessage(pkt_data,False)
        __messagePacketHandler(packet)

    # parse info messages
    if packetType==INFO_TYPE:
        packet=__parseMessage(pkt_data,True)
        __infoPacketHandler(packet)

    #      parse service packets
    if packetType==SERVICE_TYPE:
        packet=__parseServicePacket(pkt_data,True)
        __servicePacketHandler(packet)

    #      parse file packets
    if packetType==FILE_TYPE:
        packet=__parseFilePacket(pkt_data,False)
        __filePacketHandler(packet)

    #      parse request packets
    if packetType==REQUEST_TYPE:
        packet=__parseRequestPacket(pkt_data,False)
        __requestPacketHandler(packet)
