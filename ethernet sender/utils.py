import uuid
import json
import crcmod
from constants import *

class Utils:
    @staticmethod
    def getMyMac():
        myMac=hex(uuid.getnode())
        if len(myMac[2:(len(myMac)-1)])==11:
            return "0"+myMac[2:(len(myMac)-1)]
        return myMac[2:(len(myMac)-1)]

    @staticmethod
    def printPacket(packet):
        print "---------------------------------"
        print json.dumps(packet, indent = 4)

    @staticmethod
    def fillBytes(data,bytesCount):
        data=str(data)
    	if len(data)!=bytesCount*2:
    		fillStr="0"*(bytesCount*2-len(data))
    		result=fillStr+data
    		return result

    @staticmethod
    def getLenght(data):
        return hex(len(data)/2)[2:]

    @staticmethod
    def calculateCrc(hexData):
        crc16 = crcmod.mkCrcFun(0x18005, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        return hex(crc16(hexData.decode("hex")))[2:]
   
    @staticmethod
    def checkCrc(packet):
        currentCrc=packet["crc"]
        packetType=packet["type"]

        # check crc of service packet
        if packetType==SERVICE_TYPE:
            newCrc=Utils.calculateCrc(packet["lenght"]+packet["packetCount"]+packet["fileId"]+packet["fileSize"]+packet["filename"])
            if Utils.fillBytes(newCrc,4)!=currentCrc:
                return False
            else:
                return True

        # check crc of file packet
        if packetType==FILE_TYPE:
            newCrc=Utils.calculateCrc(packet["lenght"]+packet["number"]+packet["fileId"]+packet["data"])
            if Utils.fillBytes(newCrc,4)!=currentCrc:
                return False
            else:
                return True

        # check crc of request packet
        if packetType==REQUEST_TYPE:
            newCrc=Utils.calculateCrc(packet["fileId"]+packet["packetNumber"])
            if Utils.fillBytes(newCrc,4)!=currentCrc:
                return False
            else:
                return True
        
    		
