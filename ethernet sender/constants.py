# packet types
MESSAGE_TYPE="5401"
SERVICE_TYPE="5402"
FILE_TYPE="5403"
INFO_TYPE="5404"
REQUEST_TYPE="5405"
SCREEN_TYPE="5406"

# sizes
MAX_LENGHT=1024
MESSAGE_HEAD_SIZE=24
FILE_HEAD_SIZE=26


WAITING_PACKETS_TIMEOUT=5 # in sec

# templates
TEXT_PACKET_TEMPL = "%(dst_mac)s%(src_mac)s%(type)s%(lenght)s%(number)s%(data)s"
SERVICE_PACKET_TEMPL="%(dst_mac)s%(src_mac)s%(type)s%(crc)s%(lenght)s%(packetCount)s%(fileId)s%(fileSize)s%(filename)s"
FILE_PACKET_TEMPL="%(dst_mac)s%(src_mac)s%(type)s%(crc)s%(lenght)s%(number)s%(fileId)s%(data)s"
REQUEST_PACKET_TEMPL="%(dst_mac)s%(src_mac)s%(type)s%(crc)s%(fileId)s%(packetNumber)s"
SCREEN_PACKET_TEMPL="%(dst_mac)s%(src_mac)s%(type)s%(lenght)s%(number)s%(lastNumber)s%(data)s"

onlineUsersDict={"Broadcast":"ffffffffffff"}

USER_NAME="Roland"
